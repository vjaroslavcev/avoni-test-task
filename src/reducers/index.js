import data from '../../data/';
import Constants from '../constants/';



const MainReducer = (state = data, action) => {
  switch (action.type) {
    case Constants.SELECT_ODD :
      return state.map( odd => 
        odd.id === action.payload.id ? 
        { ...odd, selected: true , selectedDate: Date.now() } : odd
      )
    
    case Constants.UPDATE_VALUE :
      return state.map( odd =>
        odd.id === action.payload.id ? 
        { ...odd, value: action.payload.value } : odd
      )    

    case Constants.UPDATE_BET_AMOUNT :
      return state.map( odd =>
        odd.id === action.payload.id ? 
        { ...odd, bet: action.payload.bet } : odd
      )
        
    default:
      return state;
  }
}



export default MainReducer;