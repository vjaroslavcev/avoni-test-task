import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './containers/App.jsx';
import MainReducer from './reducers';
import './main.styl';


const store = createStore(MainReducer);


render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.querySelector('#mounting-point')
);