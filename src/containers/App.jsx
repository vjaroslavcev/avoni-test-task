import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectOdd, updateValue, updateBetAmount } from '../actions/index';
import Odd from '../components/Odd.jsx';



class App extends Component {
  componentDidMount() {
    let _that = this;
    const socket = window.io.connect('http://localhost:3000');
    socket.on('update', (msg) => _that.props.updateValue(msg))
  }


  renderList() {
    return this.props.odds.map( odd => this.renderOdd(odd) );
  }


  renderSelectedList() {
    let selectedList = this.props.odds.filter( odd => odd.selected);
    selectedList.sort((a, b) => b.selectedDate - a.selectedDate);
    return selectedList.map( odd => this.renderOdd(odd) );
  }


  renderOdd(odd) {
    return (
      <Odd 
        key={odd.id} 
        odd={odd}
        selectOdd={() => this.props.selectOdd(odd)}
        updateValue={this.props.updateValue}
        updateBetAmount={this.props.updateBetAmount}
      />
    );
  }


  render() {
    return (
      <div id='app'>
        <section className='odds'>
          <h1>List of all odds</h1>
          <ul>
            {this.renderList()}
          </ul>  
        </section>
        <section className='betslip'>
          <h1>Selected odds list (Betslip)</h1>
          <ul>
            {this.renderSelectedList()}
          </ul>
        </section>
      </div>
    );
  }
}



App.propTypes = {
  odds: PropTypes.array.isRequired,
  selectOdd: PropTypes.func.isRequired,
  updateValue: PropTypes.func.isRequired,
  updateBetAmount: PropTypes.func.isRequired
}



function mapStateToProps(state) {
  return {
    odds: state
  };
}



function mapDispatchToProps(dispatch) {
  return bindActionCreators({ 
    selectOdd,
    updateValue,
    updateBetAmount
   }, dispatch);
}

 

export default connect(
  mapStateToProps, 
  mapDispatchToProps 
)(App);