import React, { Component , PropTypes } from 'react';
import './Odd.styl';



class Odd extends Component {
  constructor() {
    super()
    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


  handleBlur(e) {
    if(e.target.value) this.props.selectOdd(this.props)
  }


  handleChange(e) {
    this.props.updateBetAmount({
      id: this.props.odd.id,
      bet: e.target.value
    });
  }


  render() {
    let betAmount = this.props.odd.bet || '';
    return(
      <li className='odd'>
        <span 
          className='odd-value'
          onClick={this.props.selectOdd}
        >{this.props.odd.value}</span>
        
        <input 
          className='input'
          type='number'
          placeholder='Bet amount'
          onBlur={this.handleBlur}
          onChange={this.handleChange}
          value={betAmount}
        />
      </li>
    );
  }
}


Odd.propTypes = {
  selectOdd: PropTypes.func.isRequired,
  updateBetAmount: PropTypes.func.isRequired,
  odd: PropTypes.object.isRequired
}


export default Odd;