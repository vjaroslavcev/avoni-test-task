import keyMirror from 'keymirror';


export default keyMirror({
  SELECT_ODD: null,
  UPDATE_VALUE: null,
  UPDATE_BET_AMOUNT: null,
});