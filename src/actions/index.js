import Constants from '../constants/';



// ACTION CREATORS
export function selectOdd(odd) {
  return {
    type: Constants.SELECT_ODD,
    payload: odd
  };
}


export function updateValue(odd) {
  return {
    type: Constants.UPDATE_VALUE,
    payload: odd
  };
}


export function updateBetAmount(odd) {
  return {
    type: Constants.UPDATE_BET_AMOUNT,
    payload: odd
  };
}
