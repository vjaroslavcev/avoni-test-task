var express = require('express');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server)
var data = require('./data/index.js');
var path = require('path');



server.listen(3000);

app.use(express.static(path.join(__dirname, 'build')));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});



io.on('connection', function(socket){
  var interval = setInterval(function(){
    sendUpdate(socket)
  }, getRandomInt() * 1000);

  socket.on('disconnect', function () {
    clearInterval(interval);
  });
});



var sendUpdate = function(socket) {
  var odd = getRandomOdd(getRandomInt(data.length-1, 0));
  socket.emit('update', {id: odd.id, value: getRandomInt()});
}


var getRandomInt = function (max, min) {
  var max = max || data.length;
  var min = min || 1;
  var number = Math.floor(Math.random() * (max - min + 1)) + min;
  return number;
}

var getRandomOdd = function(id) {
  return data[id]
}