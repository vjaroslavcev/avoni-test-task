var Config = {
  entry: './src/main.jsx',
  output: {
    path: __dirname,
    filename: './build/bundle.js'
  },
  devServer: {
    inline: true,
    port: 9999
  },
  module: {
    preLoaders: [],
    loaders: [
      {
        test: /.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ["react", "es2015", "stage-0"]
        }
      },      
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ["react", "es2015", "stage-0"]
        }
      },
      { 
        test: /\.styl$/, 
        loader: 'style-loader!css-loader!stylus-loader'
      }
    ]
  }
};


module.exports = Config;
